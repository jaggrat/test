# android-test
This is the android test
					
I have used following tech-stack to develop this application

- [x] MVVM: Used MVVM with ViewModel and LiveData to maintain updated state of UI during configuration change.
- [x] Kotlin: Used kotlin for business, view, and test cases 100%.
- [x] RxJava : To fetch the response from API.
- [x] Retrofit : Called API with query params.
- [x] Dagger : Dependency injector.
- [x] Mockito : Tested ViewModel since it is handling local logic of fav. Used mockito to mock the repository.The code coverage for ViewModel is 85%.
- [x] Espresso : To test UI.
- [x] Offline : A toast message is shown to user when there is no internet.

Note: If I had more time I could have done the better job in UI.

Espresso Test - 
![alt text](ViewModelTest.png)

![alt text](MainActivityTest.png)

![alt text](DetailsActivityTest.png)

