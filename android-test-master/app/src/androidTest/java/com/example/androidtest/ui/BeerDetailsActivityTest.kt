package com.example.androidtest.ui

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.example.androidtest.R
import com.example.androidtest.models.BeerDetails
import com.example.androidtest.utils.DATA_KEY
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@LargeTest
@RunWith(AndroidJUnit4::class)
class DetailsActivityTest {

    @get: Rule
    var mActivityRule: ActivityTestRule<BeerDetailsActivity> =
        object : ActivityTestRule<BeerDetailsActivity>(BeerDetailsActivity::class.java) {

            override fun getActivityIntent(): Intent {
                val resultData = Intent()
                resultData.putExtra(
                    DATA_KEY,
                    BeerDetails(
                        "Beer 1",
                        1,
                        "https://images.punkapi.com/v2/keg.png",
                        "This is details",
                        "A, B, C",
                        "H1, H2",
                        "FP1 , FP2, FP3"
                    )
                )
                return resultData
            }
        }

    @Before
    fun setUp() {

    }

    @After
    fun tearDown() {
    }


    @Test
    fun testDisplayName() {
        onView(withText("Beer 1")).check(matches(isDisplayed()))
        onView(withId(R.id.tv_detail_name)).check(matches(withText("Beer 1")))
    }

    @Test
    fun testDisplayDetails() {
        onView(withText("This is details")).check(matches(isDisplayed()))
        onView(withId(R.id.tv_detail_details)).check(matches(withText("This is details")))

    }

    @Test
    fun testDisplayMalts() {
        onView(withText("A, B, C")).check(matches(isDisplayed()))
        onView(withId(R.id.tv_detail_malts)).check(matches(withText("A, B, C")))

    }

    @Test
    fun testDisplayHops() {
        onView(withText("H1, H2")).check(matches(isDisplayed()))
        onView(withId(R.id.tv_detail_hops)).check(matches(withText("H1, H2")))

    }

}