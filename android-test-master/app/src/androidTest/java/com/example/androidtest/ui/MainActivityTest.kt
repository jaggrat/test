package com.example.androidtest.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import com.example.androidtest.R
import com.example.androidtest.ui.adapter.ListAdapter
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class MainActivityTest {

    @get: Rule
    var activityTestRule: ActivityTestRule<MainActivity> =
        ActivityTestRule(MainActivity::class.java)


    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun test_journey() {
        try {
            Thread.sleep(3000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        onView(withId(R.id.recycler_view)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ListAdapter.ViewHolder>(0, click())
        )
        onView(withId(R.id.recycler_view)).check(doesNotExist())
        onView(withId(R.id.tv_detail_name)).check(matches(isDisplayed()))
    }
}