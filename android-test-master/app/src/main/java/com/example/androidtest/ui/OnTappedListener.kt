package com.example.androidtest.ui

import com.example.androidtest.models.BeerInformation

interface OnTappedListener {
    fun onItemTapped(beerInformation: BeerInformation)
}