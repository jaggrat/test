package com.example.androidtest.ui

import android.graphics.Color
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidtest.models.BeerDetails
import com.example.androidtest.models.BeerInformation
import com.example.androidtest.models.BeerResponse
import com.example.androidtest.repo.DataDownloadListener
import com.example.androidtest.repo.Repo
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class MainViewModel @Inject constructor(private val repo: Repo) : ViewModel() {
    var beerInformationLiveData: MutableLiveData<List<BeerInformation>> = MutableLiveData()
    private var beerResponse: List<BeerResponse> = emptyList()
    private lateinit var composite : Disposable
    fun getBeerData() {
        composite = repo.getBeerData(listener)
    }

    var listener : DataDownloadListener = object : DataDownloadListener {
        override fun onFailure(message: String) {
            beerInformationLiveData.value = emptyList()
        }

        override fun onSuccess(list: List<BeerResponse>) {
            beerResponse = list
            processBeerDataIntoInfo(list)
        }

    }

    /**
     * Finds beer details for the item.
     * @param beerInformation: Item information
     * @return Beer details
     */
    fun findBeerDetails(beerInformation: BeerInformation): BeerDetails? {
        val favList = repo.getAllFavBeer()

        val beerInformation: BeerResponse? = beerResponse.findLast {it.id == beerInformation.id }
        beerInformation?.let {
            var malts = ""
            var hops = ""
            var foodPairing = ""
            it.ingredients.malt.forEach { value -> malts += value.name + ", " }
            it.ingredients.hops.forEach { value -> hops += value.name + ", " }
            it.food_pairing.forEach { value -> foodPairing += value + ", " }
            return BeerDetails(it.name, it.id, it.image_url, it.description, malts , hops, foodPairing,it.id.toString() in favList)
        }
       return null
    }


    /**
     * Converts response model in beer information
     * @param list: List of server model
     */
    fun processBeerDataIntoInfo(list: List<BeerResponse>) {
        val favBeer = repo.getAllFavBeer()
        beerInformationLiveData.value = list.map {
            var bgColor = Color.WHITE
            if(it.id.toString() in favBeer) {
                bgColor = Color.GRAY
            }
            BeerInformation(it.name, it.id, it.image_url, it.abv, bgColor)
        }
    }

    /**
     * Updates fav list.
     * @param beerDetails: Beer details
     */
    fun updateFavList(beerDetails: BeerDetails) {
        repo.updateBeerList(beerDetails.id, beerDetails.favourite)
        processBeerDataIntoInfo(beerResponse)
    }

    override fun onCleared() {
        super.onCleared()
        composite.dispose()
    }
}