package com.example.androidtest.rest

import com.example.androidtest.models.BeerResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface RestApi {

    @GET("v2/beers")
    fun getBeersData():  Single<Response<List<BeerResponse>>>
}