package com.example.androidtest.models

import android.graphics.Color
import java.io.Serializable


data class BeerResponse(
    val id: Int,
    val name: String,
    val tagLine: String,
    val first_brewed: String,
    val description: String,
    val image_url: String,
    val abv: Double,
    val ingredients: Ingredients,
    val food_pairing: List<String>
)