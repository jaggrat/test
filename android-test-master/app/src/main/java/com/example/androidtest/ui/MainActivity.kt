package com.example.androidtest.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidtest.R
import com.example.androidtest.di.Injectable
import com.example.androidtest.models.BeerDetails
import com.example.androidtest.models.BeerInformation
import com.example.androidtest.ui.adapter.ListAdapter
import com.example.androidtest.utils.DATA_KEY
import com.example.androidtest.utils.verifyAvailableNetwork
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), Injectable, OnTappedListener {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    private val requestResultDetail = 1

    private lateinit var viewModel: MainViewModel

    private val adapter = ListAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        setContentView(R.layout.activity_main)
        showProgressing()
        setupView()
        observeBeerInformation()
        if (verifyAvailableNetwork(this)) {
            viewModel.getBeerData()
        } else {
            Toast.makeText(this, getString(R.string.internet_warning), Toast.LENGTH_SHORT).show()
        }
    }


    private fun observeBeerInformation() {
        viewModel.beerInformationLiveData.observe(this, Observer { list ->
            dismissProgressing()
            if (list != null) {
                adapter.setData(list)
            } else {
                Toast.makeText(this, getString(R.string.data_error), Toast.LENGTH_LONG).show()
            }
            dismissProgressing()
        })
    }

    /**
     * Sets up views
     */
    private fun setupView() {
        recycler_view.let {
            it.layoutManager = LinearLayoutManager(this)
            it.adapter = adapter
            it.addItemDecoration(DividerItemDecoration(it.context, RecyclerView.VERTICAL))
        }
    }

    /**
     * Show progress
     */
    private fun showProgressing() {
        LoadingDialogFragment().show(supportFragmentManager, LoadingDialogFragment.TAG)
    }

    /**
     * Dismiss progress
     */
    private fun dismissProgressing() {
        supportFragmentManager.findFragmentByTag(LoadingDialogFragment.TAG)
            ?.takeIf { it is DialogFragment }
            ?.run {
                (this as DialogFragment).dismiss()
            }
    }

    /**
     * Callback when beerInformation is selected
     * @param beerInformation: Selected Item
     */
    override fun onItemTapped(beerInformation: BeerInformation) {
        val beerDetails = viewModel.findBeerDetails(beerInformation)
        Intent(applicationContext, BeerDetailsActivity::class.java).also {
            it.putExtra(DATA_KEY, beerDetails)
            startActivityForResult(it, requestResultDetail)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == requestResultDetail) {
            if (resultCode == Activity.RESULT_OK) {
                val beerDetails = data?.getSerializableExtra(DATA_KEY) as BeerDetails
                viewModel.updateFavList(beerDetails)
            }
        }
    }
}

