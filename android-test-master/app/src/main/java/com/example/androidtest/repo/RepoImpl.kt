package com.example.androidtest.repo

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.example.androidtest.rest.RestApi
import io.reactivex.disposables.Disposable

class RepoImpl(private val restApi: RestApi, val sharedPreferences: SharedPreferences) : Repo {
    /**
     * Calls api to get data.
     * @param listener: api end point name
     */
    @SuppressLint("CheckResult")
    override fun getBeerData(listener: DataDownloadListener): Disposable {

        return restApi.getBeersData()
            .subscribeOn(io.reactivex.schedulers.Schedulers.io())
            .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
            .subscribe({ templates ->
                val list = templates.body() ?: listOf()
                listener.onSuccess(list)
            }, {
               listener.onFailure(it.localizedMessage)
            })
    }

    /**
     * Adds item in beer list otherwise deletes it from preference
     * @param beer: Beer Id
     * @param markFav: true to save the item in preference
     */
    override fun updateBeerList(beer: Int, markFav: Boolean) {
        val editor = sharedPreferences.edit()
        if(markFav) {
            editor.putInt(beer.toString(), beer)
        } else {
            editor.remove(beer.toString())
        }
        editor.apply()
    }

    /**
     * Returns all the entries from preference
     * @return List of Ids
     */
    override fun getAllFavBeer(): List<String> {
        val favList = arrayListOf<String>()
        val allEntries = sharedPreferences.all
        for (entry in allEntries.entries) {
           favList.add(entry.component1())
        }

        return favList
    }
}