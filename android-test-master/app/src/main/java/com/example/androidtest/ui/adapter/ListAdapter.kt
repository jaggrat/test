package com.example.androidtest.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidtest.R
import com.example.androidtest.models.BeerInformation
import com.example.androidtest.ui.OnTappedListener
import com.example.androidtest.utils.loadImage

class ListAdapter(private val listener: OnTappedListener) : RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtName: TextView = itemView.findViewById(R.id.tv_name)
        val txtAbv: TextView = itemView.findViewById(R.id.tv_abv)
        val ivIcon: ImageView = itemView.findViewById(R.id.iv_icon)
        val rlParent: RelativeLayout = itemView.findViewById(R.id.rl_parent)

    }

    private val beerInfoList = mutableListOf<BeerInformation>()

    override fun getItemCount(): Int = beerInfoList.size

    fun setData(list: List<BeerInformation>?) {
        list?.let {
            beerInfoList.clear()
            beerInfoList.addAll(list)
            notifyDataSetChanged()
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val beerInformation: BeerInformation = beerInfoList[position]
        holder.txtName.text = beerInformation.name
        holder.txtAbv.text = beerInformation.abv.toString()
        holder.rlParent.setBackgroundColor(beerInformation.backgroundColor)
        holder.loadImage(holder.ivIcon, beerInformation.imageUrl)
        holder.itemView.setOnClickListener {listener.onItemTapped(beerInformation) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ViewHolder(v)
    }
}