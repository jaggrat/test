package com.example.androidtest.models

import java.io.Serializable


data class BeerDetails(
    val name: String,
    val id: Int, val imageUrl: String,
    val details: String, val malts: String,
    val hops: String,
    val foodPairing: String,
    var favourite: Boolean = false
) : Serializable