package com.example.androidtest.models


data class Ingredients(val malt: List<Malts>, val hops: List<Hops>)

data class Malts(val name: String)
data class Hops(val name: String, val attribute: String)
