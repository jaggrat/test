package com.example.androidtest.models

import android.graphics.Color
import java.io.Serializable


data class BeerInformation(
    val name: String, val id: Int, val imageUrl: String, val abv: Double,
    var backgroundColor: Int = Color.WHITE
)
