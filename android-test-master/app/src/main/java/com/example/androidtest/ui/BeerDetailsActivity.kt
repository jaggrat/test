package com.example.androidtest.ui

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.widget.ImageView
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.androidtest.R
import com.example.androidtest.models.BeerDetails
import com.example.androidtest.utils.DATA_KEY
import kotlinx.android.synthetic.main.activity_beer_details.*

class BeerDetailsActivity : AppCompatActivity() {
    private var details: BeerDetails? = null
    lateinit var switch: Switch


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer_details)
        if (intent.hasExtra(DATA_KEY)) {
            details = intent?.getSerializableExtra(DATA_KEY) as BeerDetails
            details.let {
                setUpViewsWithInfo(details)
                setProfileImage(details)
            }
        } else {
            Toast.makeText(this, getString(R.string.data_error), Toast.LENGTH_SHORT).show()
            finish()
        }

    }

    /**
     * Renders beerDetails information
     * @param beerDetails: User info
     */
    private fun setUpViewsWithInfo(beerDetails: BeerDetails?) {

        beerDetails?.let {
            tv_detail_name.text = it.name
            tv_detail_details.text = it.details
            tv_detail_malts.text = it.malts
            tv_detail_hops.text = it.hops
            tv_detail_food_pairing.text = it.foodPairing
            switch = findViewById(R.id.btn_details_fav)
            switch.isChecked = beerDetails.favourite
        }
    }

    /**
     * Sets profile pic for beerDetails
     * @param beerDetails: User info
     */
    private fun setProfileImage(beerDetails: BeerDetails?) {
        beerDetails?.imageUrl.let {
            val profileImage = findViewById<ImageView>(R.id.iv_details_image)
            val placeholder = ColorDrawable(Color.WHITE)
            val requestOptions = RequestOptions
                .placeholderOf(placeholder)
                .fitCenter()
                .optionalCenterCrop()

            Glide.with(this)
                .load(it)
                .apply(requestOptions)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(profileImage)
        }

    }

    override fun onBackPressed() {
        details?.favourite = switch.isChecked
        val resultIntent = Intent()
        resultIntent.putExtra(DATA_KEY, details)
        setResult(RESULT_OK, resultIntent)
        finish()
        super.onBackPressed()
    }
}
