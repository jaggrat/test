package com.example.androidtest.repo

import io.reactivex.disposables.Disposable

interface Repo {

    fun getBeerData(listener: DataDownloadListener): Disposable
    fun updateBeerList(beer: Int, markFav: Boolean)
    fun getAllFavBeer(): List<String>
}