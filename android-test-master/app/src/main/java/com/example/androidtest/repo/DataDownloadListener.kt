package com.example.androidtest.repo

import com.example.androidtest.models.BeerResponse

interface DataDownloadListener {
    fun onSuccess(list: List<BeerResponse>)
    fun onFailure (message: String)
}