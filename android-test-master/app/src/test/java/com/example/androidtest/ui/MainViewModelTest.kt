package com.example.androidtest.ui

import android.graphics.Color
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidtest.models.*
import com.example.androidtest.repo.Repo
import com.example.androidtest.repo.RepoImpl
import com.nhaarman.mockito_kotlin.whenever
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNull
import org.junit.After
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class MainViewModelTest {

    private lateinit var repo: Repo
    private lateinit var viewModel: MainViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        repo = Mockito.mock(RepoImpl::class.java)
        whenever(repo.getAllFavBeer()).thenReturn(arrayListOf("1"))
        viewModel = MainViewModel(repo)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun test_beer_data() {

        viewModel.processBeerDataIntoInfo(getMockData())

        assertEquals(viewModel.beerInformationLiveData.value?.size, 3)
        assertEquals(viewModel.beerInformationLiveData.value?.get(0)?.name, "Beer 1")
        assertEquals(viewModel.beerInformationLiveData.value?.get(1)?.name, "Beer 2")
        assertEquals(viewModel.beerInformationLiveData.value?.get(2)?.name, "Beer 3")
    }


    @Test
    fun test_beer_data_error() {
        viewModel.listener.onFailure("Data Error")

        assertEquals(viewModel.beerInformationLiveData.value?.size, 0)
    }

    @Test
    fun test_beer_details() {
        viewModel.listener.onSuccess(getMockData())
        val beerDetails =
            viewModel.findBeerDetails(viewModel.beerInformationLiveData.value!!.get(0))

        assertEquals(beerDetails!!.name, "Beer 1")
        assertEquals(beerDetails!!.details, "This is description")

        assertNotEquals(
            viewModel.beerInformationLiveData.value?.get(1)?.backgroundColor,
            Color.GRAY
        )
    }

    @Test
    fun test_beer_details_not_found() {
        viewModel.listener.onSuccess(getMockData())
        val beerDetails =
            viewModel.findBeerDetails(BeerInformation("Random", 100, "sample", 4.3,10020192))

        assertNull(beerDetails)
    }

    @Test
    fun test_add_beer_in_fav() {
        viewModel.listener.onSuccess(getMockData())
        val details = viewModel.findBeerDetails(viewModel.beerInformationLiveData.value!!.get(0))
        details!!.favourite = true
        viewModel.updateFavList(details)
        assertEquals(viewModel.beerInformationLiveData.value?.size, 3)
        assertEquals(viewModel.beerInformationLiveData.value?.get(0)?.name, "Beer 1")
        assertEquals(viewModel.beerInformationLiveData.value?.get(0)?.backgroundColor, -7829368)

        assertEquals(viewModel.beerInformationLiveData.value?.get(1)?.name, "Beer 2")
        assertEquals(viewModel.beerInformationLiveData.value?.get(1)?.backgroundColor, -1)

        assertEquals(viewModel.beerInformationLiveData.value?.get(2)?.name, "Beer 3")
        assertEquals(viewModel.beerInformationLiveData.value?.get(2)?.backgroundColor, -1)

    }


    private fun getMockData(): List<BeerResponse> {
        val beer1 = BeerResponse(
            1,
            "Beer 1",
            "Best Beer 1",
            "1989",
            "This is description",
            "http://sample.png",
            10.4,
            Ingredients(
                arrayListOf(Malts("Malt 1"), Malts("Malt 2")),
                arrayListOf(Hops("Hops 1", "bitter"), Hops("Hops 2", "dry"))
            ),
            arrayListOf("Chicken Nuggets 1")
            )

        val beer2 = BeerResponse(
            2,
            "Beer 2",
            "Best Beer 2",
            "1990",
            "This is description 2",
            "http://sample.png",
            10.4,
            Ingredients(
                arrayListOf(Malts("Malt 2.1"), Malts("Malt 2.2")),
                arrayListOf(Hops("Hops 2.1", "bitter"), Hops("Hops 2.2", "dry"))
            ),
            arrayListOf("Chicken Nuggets 2")
        )

        val beer3 = BeerResponse(
            3,
            "Beer 3",
            "Best Beer 3",
            "1991",
            "This is description 3",
            "http://sample.png",
            10.4,
            Ingredients(
                arrayListOf(Malts("Malt 3.1"), Malts("Malt 3.2")),
                arrayListOf(Hops("Hops 3.1", "bitter"), Hops("Hops 3.2", "dry"))
            ),

            arrayListOf("Chicken Nuggets 3")
        )

        return arrayListOf(beer1, beer2, beer3)
    }

    private fun getMockDataWithEmptyIngredientsInfo(): List<BeerResponse> {
        val beer1 = BeerResponse(
            1,
            "Beer 1",
            "Best Beer 1",
            "1989",
            "This is description",
            "http://sample.png",
            10.4,
            Ingredients(
                arrayListOf(), arrayListOf()
            ),
            arrayListOf("Chicken Nuggets")
        )

        val beer2 = BeerResponse(
            2,
            "Beer 2",
            "Best Beer 2",
            "1990",
            "This is description 2",
            "http://sample.png",
            10.4,
            Ingredients(
                arrayListOf(), arrayListOf()
            ),

            arrayListOf()
        )

        val beer3 = BeerResponse(
            3,
            "Beer 3",
            "Best Beer 3",
            "1991",
            "This is description 3",
            "http://sample.png",
            10.4,
            Ingredients(
                arrayListOf(), arrayListOf()
            ),

            arrayListOf()
        )

        return arrayListOf(beer1, beer2, beer3)
    }
}